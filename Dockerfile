ARG BASE_IMG

FROM ${BASE_IMG}
LABEL org.opencontainers.image.authors="texnoman@itmo.com"

SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND noninteractive

ENV GUI false

RUN apt-get update

RUN cd src && git clone https://github.com/be2rlab/rbcar_sim.git\
           && git clone https://gitlab.com/beerlab/models.git
COPY . src/bachelor_semifinal_scene

RUN catkin build

HEALTHCHECK --interval=20s --timeout=1s --retries=3 --start-period=20s CMD if [[ $(rostopic list 2> /dev/null | wc -c) > 0 ]]; then exit 0; fi;
ENTRYPOINT ["/bin/bash", "-ci", "roslaunch bachelor_semifinal_scene start_scene.launch gui:=${GUI}"]